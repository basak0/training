<?php
/**
 * Created by PhpStorm.
 * User: nikul
 * Date: 14.09.2018
 * Time: 21:47
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
<?php $form = ActiveForm::begin(); ?>


    <?= $form->field($model, 'email') ?>

    <div class="form-group">
        <?= Html::submitButton('Подтвердить', ['class' => 'btn btn-primary']) ?>
    </div>

<?php ActiveForm::end(); ?>