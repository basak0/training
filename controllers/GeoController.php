<?php
/**
 * Created by PhpStorm.
 * User: nikul
 * Date: 17.09.2018
 * Time: 19:38
 */

namespace app\controllers;


use app\models\Geo;
use yii\web\Controller;
use Yii;

class GeoController extends Controller
{
    //создаем подключение к БД
    public function actionCities()
    {
            $modelCity = new Geo();
            return $this->render('cities', ['model' => $modelCity]);
    }
    public function actionRegions()
    {
        $modelRegion = new Geo();
        return $this->render('regions', ['model' => $modelRegion]);
    }
    public function actionCountries()
    {
        $modelCountry = new Geo();
        return $this->render('countries', ['model' => $modelCountry]);
    }


}
