<?php

use yii\db\Migration;

/**
 * Class m180916_134236_subscribe
 */
class m180916_134236_subscribe extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $tableOptions = null;
        if ($this->db->driverName === 'mysql') {
            $tableOptions = 'CHARACTER SET utf8 COLLATE utf8_general_ci ENGINE=InnoDB';
        }
        $this->createTable('{{%q_subscribers}}', [
            'id' => $this->primaryKey(),
            'email' => $this->string()->notNull(),
        ], $tableOptions);
    }

    public function safeDown()
    {
        $this->dropTable('{{%q_subscribers}}');
    }
}