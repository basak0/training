<?php

use yii\db\Migration;

/**
 * Class m180913_171320_sxgeo
 */
class m180913_171320_sxgeo extends Migration
{



    // Use up()/down() to run migration code without a transaction.
    public function safeUp()
    {
        $this->createTable('sxgeo_cities', [
                'id'=>$this->primaryKey()->notNull()->unsigned(),
                'region_id'=>$this->Integer()->unsigned()->notNull(),
                'name_ru'=>$this->string(128)->notNull(),
                'name_en'=>$this-> string(128)->notNull(),
                'lat'=>$this-> decimal(10,5)->notNull(),
                'lon'=>$this-> decimal(10,5)->notNull(),
                'okato'=>$this-> string(20)->notNull(),
            ]
        );
        $massive=file("storage/sxgeo/city.tsv");
        foreach ($massive as $key => $val)
        {
            $values=explode("\t", $val);
            $this->insert('sxgeo_cities', [
                'id' => $values[0],
                'region_id' => $values[1],
                'name_ru' => $values[2],
                'name_en' => $values[3],
                'lat' => $values[4],
                'lon' => $values[5],
                'okato' => $values[6],
            ]);
        }

        $this->createTable('sxgeo_regions',[
                'id'=>$this->primaryKey()->notNull()->unsigned(),
                'iso'=>$this->string(8)->notNull(),
                'country'=>$this->string(2)->notNull(),
                'name_ru'=>$this-> string(128)->notNull(),
                'name_en'=>$this-> string(128)->notNull(),
                'timezone'=>$this-> string(30)->notNull(),
                'okato'=>$this-> string(4)->notNull(),
            ]
        );
        $massive=file("storage/sxgeo/region.tsv");
        foreach ($massive as $key => $val)
        {
            $values=explode("\t", $val);
            $this->insert('sxgeo_regions', [
                'id' => $values[0],
                'iso' => $values[1],
                'country' => $values[2],
                'name_ru' => $values[3],
                'name_en' => $values[4],
                'timezone' => $values[5],
                'okato' => $values[6],
            ]);
        }

        $this->createTable('sxgeo_country',[
            'id'=>$this->primaryKey()->notNull()->unsigned(),
            'iso'=>$this->string(2)->notNull(),
            'continent'=>$this->string(2)->notNull(),
            'name_ru'=>$this-> string(128)->notNull(),
            'name_en'=>$this-> string(128)->notNull(),
            'lat'=>$this-> decimal(6,2)->notNull(),
            'lon'=>$this-> decimal(6,2)->notNull(),
            'timezone'=>$this-> string(30)->notNull(),
        ]);
        $massive=file("storage/sxgeo/country.tsv");
        foreach ($massive as $key => $val)
        {
            $values=explode("\t", $val);
            $this->insert('sxgeo_country', [
                'id' => $values[0],
                'iso' => $values[1],
                'continent' => $values[2],
                'name_ru' => $values[3],
                'name_en' => $values[4],
                'lat' => $values[5],
                'lon' => $values[6],
                'timezone' => $values[7],
            ]);
        }


    }

    public function safeDown()
    {
        $this->dropTable('sxgeo_cities');
        $this->dropTable('sxgeo_regions');
        $this->dropTable('sxgeo_country');

        return false;
    }

}
