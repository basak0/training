<?php

use yii\db\Migration;

/**
 * Class m180905_183605_superproviders
 */
class m180905_183605_superproviders extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {

    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {


        return false;
    }


    // Use up()/down() to run migration code without a transaction.
    public function up()
    {
        $this->createTable('q-superproviders', [
        'id' => $this->primaryKey(),
        'name' => $this->string()->notNull(),

    ]);
    }

    public function down()
    {
        $this->dropTable('q-superproviders');


        return false;
    }

}
