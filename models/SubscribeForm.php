<?php
/**
 * Created by PhpStorm.
 * User: nikul
 * Date: 16.09.2018
 * Time: 15:50
 */

namespace app\models;


use yii\db\ActiveRecord;

class SubscribeForm extends ActiveRecord

    {


        public static function tableName()
    {
        return '{{%q_subscribers}}';
    }


        public function rules()
    {
        return [
            [ 'email', 'required','message'=>'Это поле обязательно для заполнения'],
            ['email', 'email', 'message'=>'Неккоректный e-mail адрес'],
            ['email', 'unique', 'message'=>'На даный адрес оформлена подписка'],
            ['email', 'trim'],
            ['email', 'safe'],
        ];
    }
        public function attributeLabels()
    {
        return [
            'id' => 'id',
            'email' => 'Email',
        ];
    }

    }