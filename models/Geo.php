<?php
/**
 * Created by PhpStorm.
 * User: nikul
 * Date: 17.09.2018
 * Time: 21:29
 */

namespace app\models;
use yii\base\Model;
use Yii;


class Geo extends Model
{
public static function cities()
    {
        $sql ='SELECT name_ru FROM sxgeo_cities;';
        $city = Yii::$app->db->createCommand('SELECT name_ru FROM sxgeo_cities')
        ->queryColumn();
        return $city;
    }


public static function regions()
    {
        $sql ='SELECT name_ru FROM sxgeo_regions;';
        $region = Yii::$app->db->createCommand('SELECT name_ru FROM sxgeo_regions')
            ->queryColumn();
        return $region;
    }

public static function countries()
    {
        $sql ='SELECT name_ru FROM sxgeo_country;';
        $country = Yii::$app->db->createCommand('SELECT name_ru FROM sxgeo_country')
            ->queryColumn();
        return $country;
    }
}