<?php
/**
 * Created by PhpStorm.
 * User: nikul
 * Date: 14.09.2018
 * Time: 20:10
 */
/*
 * Кнопка подписки
 */
namespace common\widgets;
use Yii;
use yii\base\Widget;
use app\models\Subscription;
class SubscriptionWidget extends Widget {
    public $subscription;
    public function init() {
        $this->subscription = new Subscription();
    }
    public function run() {
        return $this->render('subscription',[
            'model' => $this->subscription,
        ]);
    }
}